<?php
/**
 * @file
 * pushlib_publications.features.inc
 */

/**
 * Implements hook_views_api().
 */
function pushlib_publications_views_api() {
  return array("api" => "3.0");
}
