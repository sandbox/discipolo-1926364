<?php
/**
 * @file
 * pushlib_publications.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function pushlib_publications_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'biblio_cover_view';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'biblio';
  $view->human_name = 'Biblio Cover view';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Biblio Covers';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'access biblio content';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['autosubmit'] = TRUE;
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '15';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['expose']['items_per_page'] = TRUE;
  $handler->display->display_options['pager']['options']['expose']['items_per_page_options'] = '5, 15, 30, 50';
  $handler->display->display_options['pager']['options']['expose']['items_per_page_options_all'] = TRUE;
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['grouping'] = array(
    0 => array(
      'field' => 'biblio_year',
      'rendered' => 1,
      'rendered_strip' => 0,
    ),
  );
  $handler->display->display_options['row_plugin'] = 'fields';
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['label'] = 'Biblio';
  $handler->display->display_options['empty']['area']['content'] = 'No Biblio records present';
  $handler->display->display_options['empty']['area']['format'] = 'filtered_html';
  /* Field: Broken/missing handler */
  $handler->display->display_options['fields']['bid']['id'] = 'bid';
  $handler->display->display_options['fields']['bid']['table'] = 'biblio';
  $handler->display->display_options['fields']['bid']['field'] = 'bid';
  $handler->display->display_options['fields']['bid']['label'] = '';
  $handler->display->display_options['fields']['bid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['bid']['element_label_colon'] = FALSE;
  /* Field: Broken/missing handler */
  $handler->display->display_options['fields']['publication_type']['id'] = 'publication_type';
  $handler->display->display_options['fields']['publication_type']['table'] = 'biblio';
  $handler->display->display_options['fields']['publication_type']['field'] = 'publication_type';
  $handler->display->display_options['fields']['publication_type']['label'] = '';
  $handler->display->display_options['fields']['publication_type']['exclude'] = TRUE;
  $handler->display->display_options['fields']['publication_type']['element_label_colon'] = FALSE;
  /* Field: : biblio_primary_contributors */
  $handler->display->display_options['fields']['biblio_primary_contributors']['id'] = 'biblio_primary_contributors';
  $handler->display->display_options['fields']['biblio_primary_contributors']['table'] = 'field_data_biblio_primary_contributors';
  $handler->display->display_options['fields']['biblio_primary_contributors']['field'] = 'biblio_primary_contributors';
  $handler->display->display_options['fields']['biblio_primary_contributors']['label'] = '';
  $handler->display->display_options['fields']['biblio_primary_contributors']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['biblio_primary_contributors']['alter']['path'] = 'biblio/contributor/[biblio_primary_contributors-target_id]';
  $handler->display->display_options['fields']['biblio_primary_contributors']['alter']['link_class'] = 'biblio-contributor';
  $handler->display->display_options['fields']['biblio_primary_contributors']['element_type'] = '0';
  $handler->display->display_options['fields']['biblio_primary_contributors']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['biblio_primary_contributors']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['biblio_primary_contributors']['empty'] = '[Anonymous]';
  $handler->display->display_options['fields']['biblio_primary_contributors']['settings'] = array(
    'link' => 0,
  );
  $handler->display->display_options['fields']['biblio_primary_contributors']['delta_offset'] = '0';
  /* Field: : biblio_year */
  $handler->display->display_options['fields']['biblio_year']['id'] = 'biblio_year';
  $handler->display->display_options['fields']['biblio_year']['table'] = 'field_data_biblio_year';
  $handler->display->display_options['fields']['biblio_year']['field'] = 'biblio_year';
  $handler->display->display_options['fields']['biblio_year']['label'] = '';
  $handler->display->display_options['fields']['biblio_year']['element_type'] = '0';
  $handler->display->display_options['fields']['biblio_year']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['biblio_year']['element_wrapper_type'] = 'span';
  /* Field: : biblio_title */
  $handler->display->display_options['fields']['biblio_title']['id'] = 'biblio_title';
  $handler->display->display_options['fields']['biblio_title']['table'] = 'field_data_biblio_title';
  $handler->display->display_options['fields']['biblio_title']['field'] = 'biblio_title';
  $handler->display->display_options['fields']['biblio_title']['label'] = '';
  $handler->display->display_options['fields']['biblio_title']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['biblio_title']['alter']['path'] = 'biblio/[bid]';
  $handler->display->display_options['fields']['biblio_title']['alter']['link_class'] = 'biblio-title';
  $handler->display->display_options['fields']['biblio_title']['element_type'] = '0';
  $handler->display->display_options['fields']['biblio_title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['biblio_title']['element_wrapper_type'] = 'span';
  $handler->display->display_options['fields']['biblio_title']['element_wrapper_class'] = 'biblio-title';
  /* Field: : biblio_title_secondary */
  $handler->display->display_options['fields']['biblio_title_secondary']['id'] = 'biblio_title_secondary';
  $handler->display->display_options['fields']['biblio_title_secondary']['table'] = 'field_data_biblio_title_secondary';
  $handler->display->display_options['fields']['biblio_title_secondary']['field'] = 'biblio_title_secondary';
  $handler->display->display_options['fields']['biblio_title_secondary']['label'] = '';
  $handler->display->display_options['fields']['biblio_title_secondary']['element_type'] = '0';
  $handler->display->display_options['fields']['biblio_title_secondary']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['biblio_title_secondary']['element_wrapper_type'] = 'span';
  /* Field: : biblio_volume */
  $handler->display->display_options['fields']['biblio_volume']['id'] = 'biblio_volume';
  $handler->display->display_options['fields']['biblio_volume']['table'] = 'field_data_biblio_volume';
  $handler->display->display_options['fields']['biblio_volume']['field'] = 'biblio_volume';
  $handler->display->display_options['fields']['biblio_volume']['label'] = '';
  $handler->display->display_options['fields']['biblio_volume']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['biblio_volume']['alter']['text'] = '[biblio_volume]: ';
  $handler->display->display_options['fields']['biblio_volume']['element_type'] = '0';
  $handler->display->display_options['fields']['biblio_volume']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['biblio_volume']['element_wrapper_type'] = 'span';
  /* Field: : biblio_pages */
  $handler->display->display_options['fields']['biblio_pages']['id'] = 'biblio_pages';
  $handler->display->display_options['fields']['biblio_pages']['table'] = 'field_data_biblio_pages';
  $handler->display->display_options['fields']['biblio_pages']['field'] = 'biblio_pages';
  $handler->display->display_options['fields']['biblio_pages']['label'] = '';
  $handler->display->display_options['fields']['biblio_pages']['element_type'] = '0';
  $handler->display->display_options['fields']['biblio_pages']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['biblio_pages']['element_wrapper_type'] = 'span';
  /* Field: Biblio: Book Cover */
  $handler->display->display_options['fields']['field_book_cover']['id'] = 'field_book_cover';
  $handler->display->display_options['fields']['field_book_cover']['table'] = 'field_data_field_book_cover';
  $handler->display->display_options['fields']['field_book_cover']['field'] = 'field_book_cover';
  $handler->display->display_options['fields']['field_book_cover']['label'] = '';
  $handler->display->display_options['fields']['field_book_cover']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_book_cover']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_book_cover']['settings'] = array(
    'image_style' => 'medium',
    'image_link' => 'content',
  );
  /* Sort criterion: : biblio_year (biblio_year) */
  $handler->display->display_options['sorts']['biblio_year_value']['id'] = 'biblio_year_value';
  $handler->display->display_options['sorts']['biblio_year_value']['table'] = 'field_data_biblio_year';
  $handler->display->display_options['sorts']['biblio_year_value']['field'] = 'biblio_year_value';
  $handler->display->display_options['sorts']['biblio_year_value']['order'] = 'DESC';
  $handler->display->display_options['sorts']['biblio_year_value']['exposed'] = TRUE;
  $handler->display->display_options['sorts']['biblio_year_value']['expose']['label'] = 'Year';
  /* Sort criterion: : biblio_title (biblio_title) */
  $handler->display->display_options['sorts']['biblio_title_value']['id'] = 'biblio_title_value';
  $handler->display->display_options['sorts']['biblio_title_value']['table'] = 'field_data_biblio_title';
  $handler->display->display_options['sorts']['biblio_title_value']['field'] = 'biblio_title_value';
  $handler->display->display_options['sorts']['biblio_title_value']['exposed'] = TRUE;
  $handler->display->display_options['sorts']['biblio_title_value']['expose']['label'] = 'Title';

  /* Display: Year */
  $handler = $view->new_display('page', 'Year', 'page');
  $handler->display->display_options['display_description'] = 'Grouped by year';
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['path'] = 'publications';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Publications';
  $handler->display->display_options['menu']['description'] = 'a list of publications';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['name'] = 'main-menu';
  $handler->display->display_options['menu']['context'] = 0;
  $export['biblio_cover_view'] = $view;

  return $export;
}
