api = "2"
core = "7.x"

projects[biblio][type] = module
projects[biblio][download][type] = git
projects[biblio][download][branch] = 7.x-2.x
projects[biblio][download][url] = http://git.drupal.org/project/biblio.git

projects[] = ctools
projects[entity] = 1.x
projects[features] = 2.x
projects[] = views
projects[] = views_bulk_operations
